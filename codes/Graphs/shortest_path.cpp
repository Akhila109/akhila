#include<iostream>
#include<cstdio>
#include<list>
#include<queue>

using namespace std;

class Graph
{
	private:
		int V;
	public:
		list<int> *adj_list;
		Graph(int V)
		{
			this->V = V;
			adj_list = new list<int>[V];
		}
		void add_edge(int a, int b);
		void print();
};

void Graph::add_edge(int a, int b)
{
	adj_list[a].push_back(b);
//	adj_list[b].push_back(a);
	return;
}

void Graph::print()
{
	int i, j;
	for(i=0;i<V;i++)
	{
		list<int>::iterator it;
		it = adj_list[i].begin();
		while(it!=adj_list[i].end())
		{
			printf("%d ",*it);
			++it;
		}
		printf("\n");
	}
	return;
}

void shortest_path(Graph g, int head, int v, int *path, int *distance)
{
	int temp, array[v] = {0};
	list<int>::iterator it;
	queue<int> myqueue;
	myqueue.push(head);
	array[head] = 1;
	distance[head] = 0;
//	printf("%d ",head );
	while(!myqueue.empty())
	{
		temp = myqueue.front();
		myqueue.pop();
		it = g.adj_list[temp].begin();
		while(it != g.adj_list[temp].end())
		{
			if(array[*it] != 1)
			{
				myqueue.push(*it);
				array[*it] = 1;
				path[*it] = temp;
				distance[*it] = distance[temp] + 1;
			//	printf("%d ", *it);
			}
			it++;
		}
	}	
	printf("\n");
	return;
}

int main()
{
	int v, e, n, i, j, a, b;
	scanf("%d",&v);
	Graph g(v);
//	int array[v] = {0};
	int path[v], distance[v];
	for(i=0;i<v;i++)
	{
		distance[i] = -1;
		path[i] = -1;
	}
	scanf("%d",&e);
	for(i=0;i<e;i++)
	{
		cin >> a;
		cin >> b;
		g.add_edge(a,b);
	}
	g.print();

	shortest_path(g, 0, v, path, distance );

	int temp;
	for(i=0;i<v;i++)
	{
		printf("Distance of %d is %d. ", i, distance[i]);
		temp = i;
		printf("Path is ");
		while(path[temp] != -1)
		{
			printf("%d ",path[temp]);
			temp = path[temp];
		}
		printf("\n");
	}

	return 0;
} 