#include<iostream>
#include<cstdio>
#include<list>
#include<queue>
#include<cstring>

using namespace std;

class Graph
{
	private:
		int V;
	public:
		list<int> *adj_list;
		Graph(int V)
		{
			this->V = V;
			adj_list = new list<int>[V];
		}
		void add_edge(int a, int b);
		void print();
};

void Graph::add_edge(int a, int b)
{
	adj_list[a].push_back(b);
//	adj_list[b].push_back(a);
	return;
}

void Graph::print()
{
	int i, j;
	for(i=0;i<V;i++)
	{
		list<int>::iterator it;
		it = adj_list[i].begin();
		while(it!=adj_list[i].end())
		{
			printf("%d ",*it);
			++it;
		}
		printf("\n");
	}
	return;
}

void topological_sorting(Graph g, int *indegree, int v)
{
	int i, temp;
	queue<int> myqueue;
	for(i = 0; i < v; i++)
		if(indegree[i] == 0)
			myqueue.push(i);
	list<int>::iterator it;
	while(!myqueue.empty())
	{
		temp = myqueue.front();
		myqueue.pop();
		printf("%d ",temp);
		it = g.adj_list[temp].begin();
		while(it != g.adj_list[temp].end())
		{
			indegree[*it]--;
		//	printf("%d\n", *it);
			if(indegree[*it] == 0)
				myqueue.push(*it);
			it++;
		}
	}	
	printf("\n");
	return;
}

int main()
{
	int v, e, n, i, j, a, b;
	scanf("%d",&v);
	Graph g(v);
	int array[v] = {0}, indegree[v] = {0};
	scanf("%d",&e);
	for(i=0;i<e;i++)
	{
		cin >> a;
		cin >> b;
		g.add_edge(a,b);
		indegree[b]++;
	}
	g.print();
	topological_sorting(g, indegree, v);
	return 0;
} 