#include<iostream>
#include<cstdio>
#include<list>

using namespace std;

class Graph
{
	private:
		int V;
	public:
		list<int> *adj_list;
		Graph(int V)
		{
			this->V = V;
			adj_list = new list<int>[V];
		}
		void add_edge(int a, int b);
		void print();
};

void Graph::add_edge(int a, int b)
{
	adj_list[a].push_back(b);
	adj_list[b].push_back(a);
	return;
}

void Graph::print()
{
	int i, j;
	for(i=0;i<V;i++)
	{
		list<int>::iterator it;
		it = adj_list[i].begin();
		while(it!=adj_list[i].end())
		{
			printf("%d ",*it);
			++it;
		}
		printf("\n");
	}
	return;
}

void dfs(Graph g, int head, int *array)
{
	array[head] = 1;
	printf("%d ",head );
	list<int>::iterator it;
	it = g.adj_list[head].begin();
	while(it != g.adj_list[head].end())
	{
		if(array[*it]!=1)
			dfs(g, *it, array);
		it++;
	}
	return;
}

int main()
{
	int v, e, n, i, j, a, b;
	scanf("%d",&v);
	Graph g(v);
	int array[v] = {0};
	scanf("%d",&e);
	for(i=0;i<e;i++)
	{
		cin >> a;
		cin >> b;
		g.add_edge(a,b);
	}
	g.print();

	dfs(g, 0, array);

	return 0;
} 