#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<list>
#include<queue>
#include<utility>

using namespace std;

struct OrderByAge
{
    bool operator() (pair<int, int> a, pair<int, int> b) { return a.second > b.second; }
};
//typedef priority_queue<int, vector<int>, OrderByAge> age_queue;

int main()
{
    priority_queue< pair<int, int>, vector< pair<int, int> >, OrderByAge> my;
    pair<int, int> foo, boo, moo;
    foo.first = 1;
    foo.second = 2;
    my.push(foo);
    boo.first = 5;
    boo.second = 6;
    my.push(boo);
    moo = my.top();
    printf("%d %d\n",moo.first, moo.second );
    my.pop();
    moo = my.top();
    printf("%d %d\n",moo.first, moo.second );
    return 0;
}