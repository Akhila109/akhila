#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<list>
#include<queue>

using namespace std;

int min(int a, int b)
{
	if(a >= b)
		return 0;
	return 1;
}

void shortest_path(int **matrix, int start, int v, int e, int *distance, int *path)
{
	queue< int > myqueue;
	int i, j, temp, visited[v]={0};
	
	distance[start] = 0;
	myqueue.push(start);
	visited[start] = 1;

	for(j=0;j<e;j++)
	{	
		if(!myqueue.empty())
		{
			temp = myqueue.front();
			myqueue.pop();
			visited[temp] = 0;
			for(i = 0; i < v; i++)
			{
				if(matrix[temp][i] != 0)
				{
					if(min(distance[temp] + matrix[temp][i], distance[i]))
					{
						distance[i] = distance[temp] + matrix[temp][i];  
						path[i] = temp;
					}
					if(visited[i] == 0)
					{
						myqueue.push(i);
						visited[i] = 1;
					}
				}
			}
		}
	}
	return;
}

int main()
{
	int v, e, i, j, a, b, weight;
	cin >> v;
	cin >> e;
	int distance[v], path[v];
	int *matrix[v];
	for(i = 0; i < v; i++)
			matrix[i] = (int *)malloc(v*sizeof(int));
	for(i = 0; i < v; i++)
		for(j = 0; j < v; j++)
			matrix[i][j] = 0;
	for(i = 0; i < v; i++)
	{
		distance[i] = 1000;		//weights can be more than 1000 na !!
		path[i] = -1;
	}
	for(i = 0; i < e; i++)
	{
		cin >> a;
		cin >> b;
		cin >> weight;
		matrix[a][b] = weight;
	}
	shortest_path(matrix, 0, v, e, distance, path);
	for(i=0;i<v;i++)
	{
		printf("Distance of %d is %d. ", i, distance[i]);
		j = i;
		printf("Path is ");
		while(path[j] != -1)
		{
			printf("%d ",path[j]);
			j = path[j];
		}
		printf("\n");
	}
	return 0;
}