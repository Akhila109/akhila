#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

void MakeSet(vector<int> &array, int v)
{
	int i;
	for(i=0;i<v;i++)
		array[i] = i;
	return;
}

int FindSet(int num, vector<int> &array)
{
	if(array[num] == num)
		return num;
	return FindSet(array[num], array);	
}

void Union(int a, int b, vector<int> &array)
{
	array[a] = b;
//	cout << " akki is a good girl" <<endl;
	return;
}

int main()
{
	int i, j, k, w, e, v;
	cin >> v;
	cin >> e;
	vector<int> sets(v);
	vector< pair<int, pair<int, int> > >  weights(e);
	for(i=0;i<e;i++)
	{	
		cin >> weights[i].second.first;
		cin >> weights[i].second.second;
		cin >> weights[i].first;
	}
	sort(weights.begin(), weights.end());
	MakeSet(sets, v);
	for(i=0;i<e;i++)
	{	
		j = FindSet(weights[i].second.first, sets);
		k = FindSet(weights[i].second.second, sets);
		if(j != k)
		{
			Union(j, k, sets);
			cout << weights[i].second.first  << " " << weights[i].second.second << endl; 
		}
	}
	return 0;
}