#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<list>
#include<queue>

#define p pair< int, pair<int, int> > 

using namespace std;

struct give_lesser
{
    bool operator() (p a, p b) { return a.first > b.first; }
};

void min_spanning_tree(int **matrix, int start, int v)
{
	priority_queue < pair<int, pair<int, int> >, vector< pair<int, pair<int, int> > >, give_lesser > myqueue;
	int i, visited[v]={0};
	pair< int, pair<int, int> > temp, temp2;
	
	visited[start] = 1;
	for(i = 0; i < v; i++)
	{
		if(matrix[start][i] != 0)
		{
			temp.first = matrix[start][i];
			temp.second.first = start;
			temp.second.second = i;
 			myqueue.push(temp);
		}
	}
	
	while(!myqueue.empty())
	{
		temp = myqueue.top();
		myqueue.pop();
		if(visited[temp.second.second])
			continue;
		visited[temp.second.second] = 1;
		for(i = 0; i < v; i++)
			cout << visited[i] << " ";
		cout << endl;
		cout << temp.second.first << " " << temp.second.second << endl;
		for(i = 0; i < v; i++)
		{
			if(matrix[temp.second.second][i] != 0 && visited[i] == 0)
			{
				temp2.first = matrix[temp.second.second][i];
				temp2.second.first = temp.second.second;
				temp2.second.second = i;
				myqueue.push(temp2);
			}
		}
	}
	return;
}

int main()
{
	int v, e, i, j, a, b, weight;
	cin >> v;
	cin >> e;
	int *matrix[v];
	for(i = 0; i < v; i++)
			matrix[i] = (int *)malloc(v*sizeof(int));
	for(i = 0; i < v; i++)
		for(j = 0; j < v; j++)
			matrix[i][j] = 0;
	for(i = 0; i < e; i++)
	{
		cin >> a;
		cin >> b;
		cin >> weight;
		matrix[a][b] = weight;
		matrix[b][a] = weight;
	}
	min_spanning_tree(matrix, 0, v);
	return 0;
}