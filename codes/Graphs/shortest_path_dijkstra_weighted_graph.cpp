#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<list>
#include<queue>

using namespace std;

int min(int a, int b)
{
	if(a > b)
		return 0;
	return 1;
}

struct give_lesser
{
    bool operator() (pair<int, int> a, pair<int, int> b) { return a.second > b.second; }
};

void shortest_path(int **matrix, int start, int v, int *distance, int *path)
{
	priority_queue< pair<int, int>, vector< pair<int, int> >, give_lesser > myqueue;
	int i, visited[v]={0};
	pair<int, int> temp, temp2;
	
	distance[start] = 0;
	temp.first = start;
	temp.second = distance[start];
	myqueue.push(temp);
	
	while(!myqueue.empty())
	{
		temp = myqueue.top();
		myqueue.pop();
		if(visited[temp.first])
			continue;
		visited[temp.first] = 1;
		for(i = 0; i < v; i++)
		{
			if(matrix[temp.first][i] != 0 && visited[i] == 0)
			{
				if(min(distance[temp.first] + matrix[temp.first][i], distance[i]))
				{
					distance[i] = distance[temp.first] + matrix[temp.first][i];  
					path[i] = temp.first;
				}
				temp2.first = i;
				temp2.second = distance[i];
				myqueue.push(temp2);
			}
		}
	}
	return;
}

int main()
{
	int v, e, i, j, a, b, weight;
	cin >> v;
	cin >> e;
	int distance[v], path[v];
	int *matrix[v];
	for(i = 0; i < v; i++)
			matrix[i] = (int *)malloc(v*sizeof(int));
	for(i = 0; i < v; i++)
		for(j = 0; j < v; j++)
			matrix[i][j] = 0;
	for(i = 0; i < v; i++)
	{
		distance[i] = 1000;		//weights can be more than 1000 na !!
		path[i] = -1;
	}
	for(i = 0; i < e; i++)
	{
		cin >> a;
		cin >> b;
		cin >> weight;
		matrix[a][b] = weight;
	}
	shortest_path(matrix, 0, v, distance, path);
	for(i=0;i<v;i++)
	{
		printf("Distance of %d is %d. ", i, distance[i]);
		j = i;
		printf("Path is ");
		while(path[j] != -1)
		{
			printf("%d ",path[j]);
			j = path[j];
		}
		printf("\n");
	}
	return 0;
}