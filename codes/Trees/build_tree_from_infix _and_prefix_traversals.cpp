#include<iostream>
#include<cstdlib>
#include<stack>

using namespace std;

typedef struct Tree_Node{
	int data;
	struct Tree_Node *left;
	struct Tree_Node *right;
}node;

class Tree{
public:
	node *Tree_Insert(node *head, int *infix, int *prefix, int begin, int end, int pos);
	void print_preorder(node *head);
};

void Tree::print_preorder(node *head)
{
	if(head == NULL)
		return;
	cout << head->data << " ";
	print_preorder(head->left);
	print_preorder(head->right);
	return;
}

node *Tree::Tree_Insert(node *head, int *infix, int *prefix, int begin, int end, int pos)
{
	if(begin > end)
		return NULL;

	int mid;

	node *temp = (node *) malloc(sizeof(node));
	temp->data = prefix[pos];
	temp->right = NULL;
	temp->left = NULL;
	head = temp;

	for(int i = begin; i <= end; i++)
		if(infix[i] == prefix[pos])
			mid = i;
	head->left = Tree_Insert(head->left, infix, prefix, begin, mid - 1, pos+1);
	head->right = Tree_Insert(head->right, infix, prefix, mid + 1, end, pos+mid-begin+1);
	return head;
}

int main()
{
	int n, i, a, j;
	node *head = NULL;
	cin >> n;
	int infix[n] = {0}, prefix[n] = {0};
	Tree tree;
	for(i = 0; i < n; i++)
		cin >> infix[i];
	for(i = 0; i < n; i++)
		cin >> prefix[i];


	head = tree.Tree_Insert(head, infix, prefix, 0, n - 1, 0);

	cout << "Preorder Traversal ";
	tree.print_preorder(head);
	cout << endl;
 	
	return 0;
}