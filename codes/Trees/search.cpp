#include<iostream>
#include<cstdlib>
#include<stack>

using namespace std;

typedef struct Tree_Node{
	int data;
	struct Tree_Node *left;
	struct Tree_Node *right;
}node;

class Tree{
public:
	node *Tree_Insert(node *head, int a);
	void print_preorder(node *head);
	int search(node *head, int a);
};

node *Tree::Tree_Insert(node *head, int a)
{
	if(head == NULL)
	{
		node *temp = (node *)malloc(sizeof(node));
		temp->data = a;
		temp->left = NULL;
		temp->right = NULL;
		return temp;
	}
	if(head->data > a)
		head->left = Tree_Insert(head->left, a);
	else
		head->right = Tree_Insert(head->right, a);
	return head;
}

void Tree::print_preorder(node *head)
{
	if(head == NULL)
		return;
	cout << head->data << " ";
	print_preorder(head->left);
	print_preorder(head->right);
	return;
}

int Tree::search(node *head, int a)
{
	node* temp;
	temp = head;
	while(temp)
	{
		if(temp->data == a)
			return 0;
		if(a > temp->data)
			temp = temp->right;
		else
			temp = temp->left;
	}
	return 1;
}

int main()
{
	int n, i, a, j, result;
	node *head = NULL;
	cin >> n;
	Tree tree;
	for(i = 0; i < n; i++)
	{
		cin >> a;
		head = tree.Tree_Insert(head, a);
	}
	cout << "Preorder Traversal ";
	tree.print_preorder(head);
	cout << endl;

	cin >> a;
	if(tree.search(head, a))
		cout << "Not Found" << endl;
	else
		cout << "Found" << endl;	
	return 0;
}