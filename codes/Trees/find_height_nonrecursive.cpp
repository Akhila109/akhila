#include<iostream>
#include<cstdlib>
#include<stack>
#include<queue>

using namespace std;

typedef struct Tree_Node{
	int data;
	struct Tree_Node *left;
	struct Tree_Node *right;
}node;

class Tree{
public:
	node *Tree_Insert(node *head, int a);
	void print_preorder(node *head);
	int find_height(node *head);
	};

node *Tree::Tree_Insert(node *head, int a)
{
	if(head == NULL)
	{
		node *temp = (node *)malloc(sizeof(node));
		temp->data = a;
		temp->left = NULL;
		temp->right = NULL;
		return temp;
	}
	if(head->data > a)
		head->left = Tree_Insert(head->left, a);
	else
		head->right = Tree_Insert(head->right, a);
	return head;
}

void Tree::print_preorder(node *head)
{
	if(head == NULL)
		return;
	cout << head->data << " ";
	print_preorder(head->left);
	print_preorder(head->right);
	return;
}

int Tree::find_height(node *head)
{
	queue<node *> myqueue;
	int count = 0;
	node *temp;
	myqueue.push(head);
	myqueue.push(NULL);
	while(!myqueue.empty())
	{
		while(myqueue.front() != NULL)
		{
			temp = myqueue.front();
			myqueue.pop();
			if(temp->right)
				myqueue.push(temp->right);
			if(temp->left)
				myqueue.push(temp->left);
		}
		count++;	
		myqueue.pop();
		if(!myqueue.empty())
			myqueue.push(NULL);	
	}
	return count;
}

int main()
{
	int n, i, a, j;
	node *head = NULL;
	cin >> n;
	Tree tree;
	for(i = 0; i < n; i++)
	{
		cin >> a;
		head = tree.Tree_Insert(head, a);
	}
	cout << "Preorder Traversal ";
	tree.print_preorder(head);
	cout << endl;

	cout << tree.find_height(head) << endl;

//	tree.delete_tree(head);
	if(!head)
		cout << "Deleted!! Yeahhhhh" << endl;
	return 0;
}