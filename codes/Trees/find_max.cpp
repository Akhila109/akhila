#include<iostream>
#include<cstdlib>
#include<stack>
#include<queue>

using namespace std;

typedef struct Tree_Node{
	int data;
	struct Tree_Node *left;
	struct Tree_Node *right;
}node;

class Tree{
public:
	node *build_tree(node *head, int n);
	void print_preorder(node *head);
	int find_max(node *head);
};

int max(int a, int b, int c)
{
	if(a > b && a > c)
		return a;
	if(b > a && b > c)
		return b;
	return c;
}

int Tree::find_max(node *head)
{
	if(!head)
		return -1000;
	int a, b, c;
	a = find_max(head->left);
	b = find_max(head->right);
	return max(a, b, head->data);
}

node *Tree::build_tree(node *head, int n)
{
	int a, i;
	node *temp;
	queue<node *> myqueue;
	for(i = 0; i < n; i++)
	{
		cin >> a;
		if(i==0)
		{
			head = (node*) malloc(sizeof(node));
			head->data = a;
			head->left = (node*) malloc(sizeof(node));
			head->left->data = -1000;
			head->left->left = NULL;
			head->left->right = NULL;
			head->right = (node*) malloc(sizeof(node));
			head->right->data = -1000;
			head->right->left = NULL;
			head->right->right = NULL;
			myqueue.push(head->left);
			myqueue.push(head->right);
		}
		else
		{
			myqueue.front()->data = a;
			myqueue.front()->left = (node*) malloc(sizeof(node));
			myqueue.front()->left->data = -1000;
			myqueue.front()->left->left = NULL;
			myqueue.front()->left->right = NULL;
			myqueue.front()->right = (node*) malloc(sizeof(node));
			myqueue.front()->right->data = -1000;
			myqueue.front()->right->left = NULL;
			myqueue.front()->right->right = NULL;
			myqueue.push(myqueue.front()->left);
			myqueue.push(myqueue.front()->right);
			myqueue.pop();

		}
	}
	while(!myqueue.empty())
	{
		free(myqueue.front());
		myqueue.front() = NULL;
		myqueue.pop();
	}
	return head;
}

void Tree::print_preorder(node *head)
{
	if(head == NULL)
		return;
	cout << head->data << " ";
	print_preorder(head->left);
	print_preorder(head->right);
	return;
}


int main()
{
	int n, i, a, j;
	node *head = NULL;
	cin >> n;
	Tree tree;
	head = tree.build_tree(head, n);

	cout << head->data << head->left->data << head->right->data ;
	cout << head->left->left->data << head->left->right->data << head->right->left->data << head->right->right->data;

	cout << "Preorder Traversal ";
//	tree.print_preorder(head);
	cout << endl;
//	a = tree.find_max(head);
	cout << a << endl;
	return 0;
}