#include<iostream>
#include<cstdlib>
#include<stack>

using namespace std;

typedef struct Tree_Node{
	int data;
	struct Tree_Node *left;
	struct Tree_Node *right;
}node;

class Tree{
public:
	node *Tree_Insert(node *head, int a);
	void print_preorder(node *head);
	int find_least_common_ancestor(node *head, int a, int b, int *found);
};

node *Tree::Tree_Insert(node *head, int a)
{
	if(head == NULL)
	{
		node *temp = (node *)malloc(sizeof(node));
		temp->data = a;
		temp->left = NULL;
		temp->right = NULL;
		return temp;
	}
	if(head->data > a)
		head->left = Tree_Insert(head->left, a);
	else
		head->right = Tree_Insert(head->right, a);
	return head;
}

void Tree::print_preorder(node *head)
{
	if(head == NULL)
		return;
	cout << head->data << " ";
	print_preorder(head->left);
	print_preorder(head->right);
	return;
}

int Tree::find_least_common_ancestor(node *head, int a, int b, int *found)
{
	int i, j;
	if(!head)
		return 0;
	if(head->data == a || head->data == b)
		return 1;
	i = find_least_common_ancestor(head->left, a, b, found);
	j = find_least_common_ancestor(head->right, a, b, found);
	if(i && j)
	{
		cout << head->data;
		*found = head->data;
		return 1;
	}
	if(i || j)
		return 1;
	return 0;
}

int main()
{
	int n, i, a, j;
	node *head = NULL;
	int *found = (int *)malloc(sizeof(int));
	cin >> n;
	Tree tree;
	for(i = 0; i < n; i++)
	{
		cin >> a;
		head = tree.Tree_Insert(head, a);
	}
	cout << "Preorder Traversal ";
	tree.print_preorder(head);
	cout << endl;
 	
 	cin >> i;
 	cin >> j;
 	i = tree.find_least_common_ancestor(head, i, j, found);
 	if(i)
 		cout << "Answer is" << *found;
 	else
 		cout << "NOT FOUND";
	cout << endl;
	return 0;
}