#include<iostream>
#include<cstdlib>
#include<stack>

using namespace std;

typedef struct Tree_Node{
	int data;
	struct Tree_Node *left;
	struct Tree_Node *right;
}node;

class Tree{
public:
	node *Tree_Insert(node *head, int a);
	void print_preorder(node *head);
	node *mirror_image(node *head);
	int compare_mirror_trees(node *head1, node *head2);
};

node *Tree::Tree_Insert(node *head, int a)
{
	if(head == NULL)
	{
		node *temp = (node *)malloc(sizeof(node));
		temp->data = a;
		temp->left = NULL;
		temp->right = NULL;
		return temp;
	}
	if(head->data > a)
		head->left = Tree_Insert(head->left, a);
	else
		head->right = Tree_Insert(head->right, a);
	return head;
}

void Tree::print_preorder(node *head)
{
	if(head == NULL)
		return;
	cout << head->data << " ";
	print_preorder(head->left);
	print_preorder(head->right);
	return;
}

node *Tree::mirror_image(node *head)
{
	if(!head)
		return NULL;
	node *temp;
	head->left = mirror_image(head->left);
	head->right = mirror_image(head->right);
	temp = head->left;
	head->left = head->right;
	head->right = temp;
	return head;
}

int Tree::compare_mirror_trees(node *head1, node *head2)
{
//	cout << head1->left->data << head2->right->data << head1->right->data << head2->left->data << endl;
	if(!head2 && !head1)
		return 1;
	if(!head1 || !head2)
		return 0;
//	cout << head1->data << head2->data << endl;
	if(head1->data != head2->data)
	{

		return 0;
	}
//	cout << head1->left->data << head2->right->data << head1->right->data << head2->left->data << endl;
	return (compare_mirror_trees(head1->left, head2->right) && compare_mirror_trees(head1->right, head2->left));
}

int main()
{
	int n, i, a, j;
	node *head = NULL;
	node *head2 = NULL;
	cin >> n;
	Tree tree;
	for(i = 0; i < n; i++)
	{
		cin >> a;
		head = tree.Tree_Insert(head, a);
	}
	cout << "Preorder Traversal ";
	tree.print_preorder(head);
	cout << endl;

	for(i = 0; i < n; i++)
	{
		cin >> a;
		head2 = tree.Tree_Insert(head2, a);
	}
	cout << "Preorder Traversal ";
	tree.print_preorder(head2);
	cout << endl;

	if(tree.compare_mirror_trees(head, head2))
		cout << "Mirror Images" << endl; 
	else
		cout << "Not Mirror Images" << endl;

	return 0;
}