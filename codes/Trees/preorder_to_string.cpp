#include<iostream>
#include<cstdlib>
#include<stack>
#include<cstring>

using namespace std;

typedef struct Tree_Node{
	char data;
	struct Tree_Node *left;
	struct Tree_Node *right;
}node;

class Tree{
public:
	node *Tree_Insert(node *head, char *s, int *pos);
	void print_preorder(node *head);
};

node *Tree::Tree_Insert(node *head, char* s, int *pos)
{
	if(s[*pos] == '\0')
		return head;

	node *temp = (node*)malloc(sizeof(node));
	temp->left = NULL;
	temp->right = NULL;
	temp->data = s[*pos];

	(*pos)++;

	if(head == NULL)
		head = temp;

	if(head->data == 'I')
	{
		if(!head->left)
			head->left = Tree_Insert(head->left, s, pos);
		if(!head->right)
			head->right = Tree_Insert(head->right, s, pos);	
	}
	return head;
}

void Tree::print_preorder(node *head)
{
	if(head == NULL)
		return;
	cout << head->data << " ";
	print_preorder(head->left);
	print_preorder(head->right);
	return;
}


int main()
{
	int n, i, a;
	int *j = (int *)malloc(sizeof(int));
	node *head;
	Tree tree;
	char s[100];
	cin >> n;
	for(i = 0, *j = 0; i < n; i++, *j = 0)
	{
		head = NULL;
		cin >> s;
		head = tree.Tree_Insert(head, s, j);
		cout << "Preorder Traversal ";
		tree.print_preorder(head);
		cout << endl;	
	}
	return 0;
}