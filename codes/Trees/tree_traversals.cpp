#include<iostream>
#include<cstdlib>
#include<stack>
#include<utility>
#include<algorithm>
#include<list>

using namespace std;

typedef struct Tree_Node{
	int data;
	struct Tree_Node *left;
	struct Tree_Node *right;
}node;

class Tree{
public:
	node *Tree_Insert(node *head, int a);
	void print_preorder(node *head);
	void print_postorder(node *head);
	void print_inorder(node *head);
	node *delete_tree(node *head);
	void print_preorder_non_recursive(node *head);
	void print_all_paths(node *head, int *array, int count);
	int find_sum_path(node *head, int total, int sum, int found);
	node *mirror_image(node *head);
	void print_vertical_paths(node* head, list< pair<int, int> > &mylist, int count);
};

node *Tree::Tree_Insert(node *head, int a)
{
	if(head == NULL)
	{
		node *temp = (node *)malloc(sizeof(node));
		temp->data = a;
		temp->left = NULL;
		temp->right = NULL;
		return temp;
	}
	if(head->data > a)
		head->left = Tree_Insert(head->left, a);
	else
		head->right = Tree_Insert(head->right, a);
	return head;
}

void Tree::print_preorder(node *head)
{
	if(head == NULL)
		return;
	cout << head->data << " ";
	print_preorder(head->left);
	print_preorder(head->right);
	return;
}

void Tree::print_postorder(node *head)
{
	if(head == NULL)
		return;
	print_postorder(head->left);
	print_postorder(head->right);
	cout << head->data << " ";
	return;
}

void Tree::print_inorder(node *head)
{
	if(head == NULL)
		return;
	print_inorder(head->left);
	cout << head->data << " ";
	print_inorder(head->right);
	return;
}

node* Tree::delete_tree(node *head)
{
	if(head == NULL)
		return NULL;
	head->left = delete_tree(head->left);
	head->right = delete_tree(head->right);
	free(head);
	head = NULL;
	return head;
}

void Tree::print_all_paths(node *head, int *array, int count)
{
	if(!head)
		return;
	array[count++] = head->data;
	if(!head->left && !head->right)
	{
		for(int i=0;i<count; i++)
			cout << array[i] << " ";
		cout << endl;
	}
	print_all_paths(head->left, array, count);
	print_all_paths(head->right, array, count);
	return;
}

void Tree::print_vertical_paths(node* head, list< pair<int, int> > &mylist, int count)
{
	if(!head)
		return;
	pair<int, int> p;
	p.first = count;
	p.second = head->data;
	mylist.push_back(p);
	print_vertical_paths(head->left, mylist, count-1);
	print_vertical_paths(head->right, mylist, count+1);
	return;
}

int Tree::find_sum_path(node *head, int total, int sum, int found)
{
	if(!head)
		return found;
	sum += head->data;
	if(sum == total)
	{
		found = 0;
		return found;
	}
	if(found)
	{
		found = find_sum_path(head->left, total, sum, found);
		found = find_sum_path(head->right, total, sum, found);
	}
	return found;
}

void Tree::print_preorder_non_recursive(node *head)
{
	node *temp;
	stack<node * > mystack;
	mystack.push(head);
	while(!mystack.empty())
	{
		temp = mystack.top();
		mystack.pop();
		if(!temp)
			continue;
		cout << temp->data << " ";	
		mystack.push(temp->right);
		mystack.push(temp->left);
	}
	return;
}

node *Tree::mirror_image(node *head)
{
	if(!head)
		return NULL;
	node *temp;
	head->left = mirror_image(head->left);
	head->right = mirror_image(head->right);
	temp = head->left;
	head->left = head->right;
	head->right = temp;
	return head;
}

int main()
{
	int n, i, a, j;
	node *head = NULL;
	cin >> n;
	Tree tree;
	list< pair<int, int> > mylist;
	for(i = 0; i < n; i++)
	{
		cin >> a;
		head = tree.Tree_Insert(head, a);
	}
	cout << "Preorder Traversal ";
	tree.print_preorder(head);
	cout << endl;
	cout << "Postorder Traversal ";	
	tree.print_postorder(head);
	cout << endl;
	cout << "Inorder Traversal ";
	tree.print_inorder(head);
	cout << endl;
	
	//Non recursive version
	node *temp = head;
	cout << "Preorder Traversal ";
	tree.print_preorder_non_recursive(temp);
	cout << endl;

	int array[100];
	tree.print_all_paths(head, array, 0);

	head = tree.mirror_image(head);

	cin >> n;
	if(tree.find_sum_path(head, n, 0, 1))
		cout << "Not found" << endl;
	else
		cout << "Found" << endl;

	tree.print_vertical_paths(head, mylist, 0);	
	mylist.sort();
	list< pair<int, int> >::iterator it = mylist.begin();
	while(it!=mylist.end())
	{
		cout << (*it).first << (*it).second << " ";
		it++;
	}
	cout << endl;

	head = tree.delete_tree(head);
	if(head == NULL)
		cout << "Deleted!! Yeahhhhh" << endl;

	return 0;
}