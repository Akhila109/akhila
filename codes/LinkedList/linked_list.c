#include<stdio.h>
#include<stdlib.h>

//Insert(i), delete(d), delete linked list(l) and listlength(anything else)
//Input i value position to insert
//Input d value to delete
//Input l to delete the whole list
//Adding a number at 1st position only should be done only in the first query

typedef struct ListNode{
	int data;
	struct ListNode *next;
}ListNode;

void Insert(ListNode **head, int num, int pos)
{
	int i = 1;
	ListNode *temp = (ListNode*) malloc(sizeof(ListNode));
	temp->data = num;
	temp->next = NULL;
	
	ListNode *p = *head;
	
	if(pos == 1)
		*head = temp;
	while(i<pos - 1 && p->next != NULL)
	{
		p = p->next;
		i+=1;
	}
	if(i == pos-1 && pos != 1)
	{
		if(p->next == NULL)
			p->next = temp;
		else
		{
			temp->next = p->next;
			p->next = temp;
		}
	}
	return;
}

void delete(ListNode **head, int num)
{
	ListNode *p = *head;
	while(p->next!= NULL && p->next->data != num)
		p = p->next;
	if(p->next == NULL)
		return;
	else if(p->next->data == num)
		p->next = p->next->next;
	return; 
}

void print(ListNode **head)
{
	ListNode *iter = *head;
	while(iter)
	{
		printf("%d ",(iter)->data );
		iter = (iter) -> next;
	}
	printf("\n"); 	//extra space printed after last entry
	return;
}

void deletelist(ListNode **head)
{
	ListNode *temp;
	ListNode *p = *head;
	while(p)
	{
		temp = p->next;
		free(p);
		p = temp;
	}
	*head = NULL;
	return;
}

ListNode *reverselist(ListNode **head)
{
	if(*head == NULL)
		return NULL;
	if((*head)->next == NULL)
		return *head;
	ListNode *t;
	t = (*head)->next;
	(*head)->next = NULL;
	ListNode *r = reverselist(&t);
	t->next = *head;
	return r;
}

int listlength(ListNode **head)
{
	int count = 0;
	ListNode *p = *head;
	while(p)
	{
		count ++ ;
		p = p->next;
	}
	return count;
}

ListNode *reverseKnodes(ListNode **head, int k)
{
	ListNode *temp = *head;
	ListNode *counter = *head;
	ListNode *prev;
	int done = 0, i, flag = 0;
	while(done!=1)
	{
		temp = counter; 
		for(i = 0; i < k - 1; i++)
		{
			if(counter!=NULL)
				counter = counter->next;
			else
			{	
				done = 1;
				break;
			}
		}
		if(done==1)
		{
			prev->next = temp;
			break;
		}
		ListNode *t = counter->next;
		counter->next = NULL;
		if(flag == 0)
		{
			*head = reverselist(&temp);
			flag++;
		}
		else
			prev->next = reverselist(&temp);
		prev = temp;
		counter = t;
	}
	return *head;
}

int main()
{
	ListNode *head;
	int tc, i, j, k, num, pos;
	char c;
	scanf("%d",&tc);
	for(i=0;i<tc;i++)
	{
		scanf("%c",&c);
		scanf("%c",&c);		
		if(c=='i')
		{
			scanf("%d",&num);
			scanf("%d",&pos);		
			Insert(&head, num, pos);
		}
		else if(c=='d')
		{
			scanf("%d",&num);
			delete(&head, num);
		}
		else if(c=='l')
		{
			deletelist(&head);
			break;
		}
		else if(c=='r')
		{
			head = reverselist(&head);
		}
		else if(c=='k')
		{
			scanf("%d",&k);
			head = reverseKnodes(&head, k);
		}
		else
			printf("%d\n",listlength(&head));
	}
	if(head)
		print(&head);
	return 0 ;
}