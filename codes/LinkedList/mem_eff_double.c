#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

typedef struct ListNode
{
	int data;
	struct ListNode *ptrdiff;
}ListNode;

void print(ListNode **head)
{
	ListNode *prev = NULL;
	ListNode *pres = *head;
	while(pres!=NULL)
	{
		printf("%d ", pres->data);
		ListNode *t = pres;
		pres = (ListNode *)((uintptr_t)pres->ptrdiff ^ (uintptr_t)prev);
		prev = t;
	}
	printf("\n");
	return;
}

void Insert(ListNode **head, int num, int pos)
{
	ListNode *temp = (ListNode *)malloc(sizeof(ListNode));
	ListNode *prev = NULL;
	ListNode *pres = *head;
	ListNode *t;
	int i;
	temp->data = num;
	temp->ptrdiff = NULL;
	if(pos == 1)
	{
		*head = temp;
		return;
	}
	for(i = 1; i < pos - 1 && (ListNode *)((uintptr_t)pres->ptrdiff ^ (uintptr_t)prev)!=NULL; i++)
	{
		t = prev;
		prev = pres;
		pres = (ListNode *)((uintptr_t)pres->ptrdiff ^ (uintptr_t)t);
	}
	if(i == pos-1)
	{
		if((ListNode *)((uintptr_t)pres->ptrdiff ^ (uintptr_t)prev) == NULL)
		{	
			temp->ptrdiff = (ListNode *)((uintptr_t)pres ^ (uintptr_t)NULL);
			pres->ptrdiff = (ListNode *)((uintptr_t)prev ^ (uintptr_t)temp);
		}
		else 
		{
			t = (ListNode *)((uintptr_t)pres->ptrdiff ^ (uintptr_t)prev);              //temp2
			pres->ptrdiff = (ListNode *)((uintptr_t)prev ^ (uintptr_t)temp);
			temp->ptrdiff = (ListNode *)((uintptr_t)pres ^ (uintptr_t)t);
			t->ptrdiff = (ListNode *)((uintptr_t)t->ptrdiff ^ (uintptr_t)pres ^ (uintptr_t)temp);
		}
	}
	return;
}

int main()
{
	int tc, i, j, num, pos;
	char c;
	ListNode *head;
	scanf("%d", &tc);
	for(i = 0; i< tc;i++)
	{
		scanf("%c", &c);
		scanf("%c", &c);
		scanf("%d", &num);
		scanf("%d", &pos);
		if(c=='i')
			Insert(&head, num, pos);
		print(&head);
	}
	return 0;
}