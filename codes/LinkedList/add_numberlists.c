#include<stdio.h>
#include<stdlib.h>

typedef struct ListNode{
	int data;
	struct ListNode *next;
}ListNode;

void Insert(ListNode **head, int num, int pos)
{
	int i = 1;
	ListNode *temp = (ListNode*) malloc(sizeof(ListNode));
	temp->data = num;
	temp->next = NULL;
	
	ListNode *p = *head;
	
	if(pos == 1)
		*head = temp;
	while(i<pos - 1 && p->next != NULL)
	{
		p = p->next;
		i+=1;
	}
	if(i == pos-1 && pos != 1)
	{
		if(p->next == NULL)
			p->next = temp;
		else
		{
			temp->next = p->next;
			p->next = temp;
		}
	}
	return;
}

void print(ListNode **head)
{
	ListNode *iter = *head;
	while(iter)
	{
		printf("%d ",(iter)->data );
		iter = (iter) -> next;
	}
	printf("\n"); 	//extra space printed after last entry
	return;
}

ListNode *reverselist(ListNode **head)
{
	if(*head == NULL)
		return NULL;
	if((*head)->next == NULL)
		return *head;
	ListNode *t;
	t = (*head)->next;
	(*head)->next = NULL;
	ListNode *r = reverselist(&t);
	t->next = *head;
	return r;
}

ListNode *Add_Number_Lists(ListNode **head1, ListNode **head2)
{
	ListNode *head3, *temp;
	ListNode *add_head; 
	int carry = 0, flag = 0;
	while(*head1 && *head2)
	{
		temp = (ListNode *)malloc(sizeof(ListNode *));
		temp->data = (((*head1)->data + (*head2)->data) + carry)%10;
		temp->next = NULL;
		if(flag == 0)
		{
			head3 = temp;
			add_head = head3;
			flag = 1;
		}
		else
		{
			(head3)->next = temp;
			head3 = (head3)->next;
		}
		carry = ((*head1)->data + (*head2)->data + carry)/10;
		*head2 = (*head2)->next;
		*head1 = (*head1)->next;
	}
	if(*head1)
	{
		
		(head3)->next = *head1;
		((head3)->next)->data = ((head3)->next)->data + carry;
	}
	if(*head2)
	{
		(head3)->next = *head2;
		((head3)->next)->data = ((head3)->next)->data + carry;
	}
	if(!(*head1) && !(*head2))
	{
		head3->next = (ListNode *)malloc(sizeof(ListNode *));
		head3->next->data = carry;
		head3->next->next = NULL;	
	}
	return add_head;
}

int main()
{
	ListNode *head1, *head2;
	int tc, i, j, k, num, pos;
	char c;
	scanf("%d",&tc);
	for(i=0;i<tc;i++)
	{
		scanf("%c",&c);
		scanf("%c",&c);		
		if(c=='i')
		{
			scanf("%d",&num);
			scanf("%d",&pos);		
			Insert(&head1, num, pos);
		}
	}
	head1 = reverselist(&head1);
	scanf("%d",&tc);
	for(i=0;i<tc;i++)
	{
		scanf("%c",&c);
		scanf("%c",&c);		
		if(c=='i')
		{
			scanf("%d",&num);
			scanf("%d",&pos);		
			Insert(&head2, num, pos);
		}
	}
	head2 = reverselist(&head2);
	print(&head1);
	print(&head2);

	ListNode *head = Add_Number_Lists(&head1, &head2);
	head = reverselist(&head);
	print(&head);
	return 0 ;
}