#include<iostream>
#include<map>
#include<stdlib.h>
#include<stdio.h>

using namespace std;

typedef struct ListNode{
	int data;
	struct ListNode *next;
	struct ListNode *random;
}ListNode;

void print(ListNode **head)
{
	ListNode *iter = *head;
	while(iter)
	{
		printf("%d ",(iter)->data );
		iter = (iter) -> next;
	}
	printf("\n");	//extra space printed at last
	return;
}

void Insert(ListNode **head, int num, int pos)
{
	int i = 1;
	ListNode *temp = (ListNode*) malloc(sizeof(ListNode));
	temp->data = num;
	temp->next = NULL;
	temp->random = NULL;
	
	ListNode *p = *head;
	
	if(pos == 1)
		*head = temp;
	while(i<pos - 1 && p->next != NULL)
	{
		p = p->next;
		i+=1;
	}
	if(i == pos-1 && pos != 1)
	{
		if(p->next == NULL)
			p->next = temp;
		else
		{
			temp->next = p->next;
			p->next = temp;
		}
	}
	return;
}

ListNode *Clone_list(ListNode **head)
{
	map<ListNode *, ListNode *> hash;
	ListNode *iter = *head;
	int i = 1;
	ListNode *new_head, *iter2;
	while(iter)
	{
		Insert(&new_head, iter->data, i);
		i++;
		iter = iter->next;
	}
	iter = *head;
	iter2 = new_head;
	while(iter)
	{
		hash[iter] = iter2;
		iter = iter->next;
		iter2 = iter2->next;
	}
	iter = *head;
	while(iter)
	{
		hash[iter]->random = hash[iter->random];
		iter = iter->next;
	}
	return new_head;
}

int main()
{
	ListNode *head, *temp;
	int tc, i, num, pos;
	char c;
	scanf("%d",&tc);
	for(i=0;i<tc;i++)
	{
		scanf("%c",&c);
		scanf("%c",&c);	
		if(c=='i')
		{
			scanf("%d",&num);
			scanf("%d",&pos);		
			Insert(&head, num, pos);
		}
	}
	temp = head;
	while(temp)
	{
		temp->random = temp->next;
		temp = temp->next;
	}
	head = Clone_list(&head);
	if(head)
		print(&head);
	return 0 ;
}