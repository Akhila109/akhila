#include<stdio.h>
#include<stdlib.h>

//DOUBLY LINKED LIST
//Insert(i), delete(d), delete linked list(l) and listlength(anything else)
//Input i value position to insert
//Input d value to delete
//Input l to delete the whole list

typedef struct ListNode{
	int data;
	struct ListNode *next;
	struct ListNode *prev;
}ListNode;

void Insert(ListNode **head, int num, int pos)
{
	int i = 1;
	ListNode *temp = (ListNode*) malloc(sizeof(ListNode));
	temp->data = num;
	temp->next = NULL;
	temp->prev = NULL;
	
	ListNode *p = *head;
	
	if(pos == 1)
		*head = temp;
	while(p->next != NULL && i<pos - 1)
	{
		p = p->next;
		i+=1;
	}
	if(i == pos-1 && pos != 1)
	{
		if(p->next == NULL)
		{
			p->next = temp;
			temp->prev = p;
		}
		else
		{
			temp->next = p->next;
			temp->prev = p;
			p->next = temp;
		}
	}

	return;
}

void delete(ListNode **head, int num)
{
	ListNode *p = *head;
	while(p->next!= NULL && p->next->data != num)		//because doubly linked,  we could have iterated till the node i.e p->data!=num
		p = p->next;
	if(p->next == NULL)
		return;
	else if(p->next->data == num)
	{
		ListNode* temp = p->next;
		p->next->next->prev = p;
		p->next = p->next->next;
		free(temp);
	}
	return; 
}

void print(ListNode **head)
{
	while((*head)->next != NULL)
	{
		printf("%d ",(*head)->data );
		*head = (*head) -> next;
	}
	printf("%d\n",(*head)->data );
	return;
}

void deletelist(ListNode **head)
{
	ListNode *temp;
	ListNode *p = *head;
	while(p)
	{
		temp = p->next;
		free(p);
		p = temp;
	}
	*head = NULL;
	return;
}

int listlength(ListNode **head)
{
	int count = 0;
	ListNode *p = *head;
	while(p)
	{
		count ++ ;
		p = p->next;
	}
	return count;
}

int main()
{
	ListNode *head;

	int tc, i, j, num, pos;
	char c;
	scanf("%d",&tc);
	for(i=0;i<tc;i++)
	{
		scanf("%c",&c);
		scanf("%c",&c);
		
		if(c=='i')
		{
			scanf("%d",&num);
			scanf("%d",&pos);		
			Insert(&head, num, pos);
		}
		else if(c=='d')
		{
			scanf("%d",&num);
			delete(&head, num);
		}
		else if(c=='l')
		{
			deletelist(&head);
			break;
		}
		else
			printf("%d\n",listlength(&head));
	}

	if(head)
		print(&head);
	return 0 ;
}