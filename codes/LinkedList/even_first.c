#include<stdio.h>
#include<stdlib.h>

typedef struct ListNode{
	int data;
	struct ListNode *next;
}ListNode;

void Insert(ListNode **head, int num, int pos)
{
	int i = 1;
	ListNode *temp = (ListNode*) malloc(sizeof(ListNode));
	temp->data = num;
	temp->next = NULL;
	
	ListNode *p = *head;
	
	if(pos == 1)
		*head = temp;
	while(i<pos - 1 && p->next != NULL)
	{
		p = p->next;
		i+=1;
	}
	if(i == pos-1 && pos != 1)
	{
		if(p->next == NULL)
			p->next = temp;
		else
		{
			temp->next = p->next;
			p->next = temp;
		}
	}
	return;
}

void print(ListNode **head)
{
	ListNode *iter = *head;
	while(iter)
	{
		printf("%d ",(iter)->data );
		iter = (iter) -> next;
	}
	printf("\n"); 	//extra space printed after last entry
	return;
}

ListNode *Seperate_even_odd(ListNode **head)
{
//	ListNode *new_head;
	ListNode *even = NULL;
	ListNode *iter = *head;
	ListNode *prev = NULL;
	while(iter)
	{
		if(iter->data % 2 == 1)
		{
			prev = iter;
			iter = iter->next;
		}
		else
		{
			if(even == NULL )
			{
				if(prev==NULL)
				{	
					*head = iter; 
					even = iter;
					prev = iter;
					iter = iter->next;
				}
				else
				{
					*head = iter; 
					even = iter;
					prev->next = iter->next;
					even->next = prev;
					iter = prev->next;
				}
				
			}
			else
			{
				prev->next = iter->next;
				iter->next = even->next;
				even->next = iter;
				iter = prev->next;
				even = even->next;
			}
		}
	}
	return *head;	
}

int main()
{
	ListNode *head;
	int tc, i, j, k, num, pos;
	char c;
	scanf("%d",&tc);
	for(i=0;i<tc;i++)
	{
		scanf("%c",&c);
		scanf("%c",&c);		
		if(c=='i')
		{
			scanf("%d",&num);
			scanf("%d",&pos);		
			Insert(&head, num, pos);
		}
	}
	head = Seperate_even_odd(&head);
	if(head)
		print(&head);
	return 0 ;
}