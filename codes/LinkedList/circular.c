#include<stdio.h>
#include<stdlib.h>

//Insert(i), delete(d), delete linked list(l) and listlength(anything else)
//Input i value position to insert
//Input d value to delete
//Input l to delete the whole list

typedef struct ListNode{
	int data;
	struct ListNode *next;
}ListNode;

void Insert(ListNode **head, int num, int pos)
{
	int i = 1;
	ListNode *temp = (ListNode*) malloc(sizeof(ListNode));
	temp->data = num;
	temp->next = *head;
	
	ListNode *p = *head;
	
	if(pos == 1)
		*head = temp;
	while(p->next != *head && i<pos - 1)
	{
		p = p->next;
		i+=1;
	}
	if(i == pos-1 && pos != 1)
	{
		if(p->next == *head)
			p->next = temp;
		else
		{
			temp->next = p->next;
			p->next = temp;
		}
	}

	return;
}

void delete(ListNode **head, int num)
{
	ListNode *p = *head;
	while(p->next!= *head && p->next->data != num)
		p = p->next;
	if(p->next == *head)
		return;
	else if(p->next->data == num)
		p->next = p->next->next;
	return; 
}

void print(ListNode **head)
{
	ListNode *p = *head;
	while(p->next != *head)
	{
		printf("%d ",p->data );
		p = p -> next;
	}
	printf("%d\n",p->data );
	return;
}

void deletelist(ListNode **head)		//To Be Done
{
	ListNode *temp;
	ListNode *p = *head;
	while(p)
	{
		temp = p->next;
		free(p);
		p = temp;
	}
	*head = NULL;
	return;
}

int listlength(ListNode **head)
{
	int count = 0;
	ListNode *p = *head;
	while(p)
	{
		count ++ ;
		p = p->next;
	}
	return count;
}

int main()
{
	ListNode *head;

	int tc, i, j, num, pos;
	char c;
	scanf("%d",&tc);
	for(i=0;i<tc;i++)
	{
		scanf("%c",&c);
		scanf("%c",&c);
		
		if(c=='i')
		{
			scanf("%d",&num);
			scanf("%d",&pos);		
			Insert(&head, num, pos);
		}
		else if(c=='d')
		{
			scanf("%d",&num);
			delete(&head, num);
		}
		else if(c=='l')
		{
			deletelist(&head);
			break;
		}
		else
			printf("%d\n",listlength(&head));
	}

	if(head)
		print(&head);
	return 0 ;
}