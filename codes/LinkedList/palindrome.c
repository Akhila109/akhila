#include<stdio.h>
#include<stdlib.h>

typedef struct ListNode{
	int data;
	struct ListNode *next;
}ListNode;

void Insert(ListNode **head, int num, int pos)
{
	int i = 1;
	ListNode *temp = (ListNode*) malloc(sizeof(ListNode));
	temp->data = num;
	temp->next = NULL;
	
	ListNode *p = *head;
	
	if(pos == 1)
		*head = temp;
	while(p->next != NULL && i<pos - 1)
	{
		p = p->next;
		i+=1;
	}
	if(i == pos-1 && pos != 1)
	{
		if(p->next == NULL)
			p->next = temp;
		else
		{
			temp->next = p->next;
			p->next = temp;
		}
	}
	return;
}

void print(ListNode **head)
{
	ListNode *iter = *head;
	while((iter)->next != NULL)
	{
		printf("%d ",(iter)->data );
		iter = (iter) -> next;
	}
	printf("%d\n",(iter)->data );
	return;
}

ListNode *reverselist(ListNode **head)
{
	if(*head == NULL)
		return NULL;
	if((*head)->next == NULL)
		return *head;
	ListNode *t;
	t = (*head)->next;
	(*head)->next = NULL;
	ListNode *r = reverselist(&t);
	t->next = *head;
	return r;
}

int CheckPalindrome(ListNode **head)
{
	ListNode *slow = *head;
	ListNode *fast = *head;
	while(fast->next!=NULL && fast->next->next != NULL)
	{
			slow = slow->next;
			fast = fast->next->next;
	}
	ListNode *iter2 = reverselist(&(slow->next));
	ListNode *iter1 = *head;
	while(iter2)
	{
		if(iter1->data != iter2->data)
			return 0;
		iter2 = iter2->next;
		iter1 = iter1->next;
	}
	return 1;
}

int main()
{
	ListNode *head;
	int tc, i, j, num, pos;
	char c;
	scanf("%d",&tc);
	for(i=0;i<tc;i++)
	{
		scanf("%c",&c);
		scanf("%c",&c);
		
		if(c=='i')
		{
			scanf("%d",&num);
			scanf("%d",&pos);		
			Insert(&head, num, pos);
		}
		else
			printf("Insert properly\n");
	}
	int result = CheckPalindrome(&head);
	if(result == 0)
		printf("NOT A PALINDROME!! :( :'(\n");
	else
		printf("YESSS... IT IS A PALINDROME!!! :) :D\n");

	return 0 ;
}
