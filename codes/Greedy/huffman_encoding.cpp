#include<iostream>
#include<cstdlib>
#include<algorithm>
#include<list>

using namespace std;

typedef struct node{
	int data;
	int frequency;
	struct node *left;
	struct node *right;
}node;

node build_tree(node head, int n, list<node> &mylist)
{
	int i;
	node temp;		//assuming maximum 1000
	while(mylist.size() > 1)
	{
		temp = (node*)malloc(sizeof(node));
		temp->right = mylist.front();
		mylist.pop_front();
		temp->left = mylist.front();
		mylist.pop_front();
		temp->data = 0;
		temp->frequency = temp->right->frequency + temp->left->frequency;
		list<node*>::iterator iter = mylist.begin();
		while(temp->frequency <= (*iter)->frequency)
			iter++;
		mylist.insert(iter, temp);
	}
	return mylist.front();
}

bool compare(node &first, node &second)
{
	return first.frequency < second.frequency;
}

int main()
{
	int i, j, f, n;
	cin >> n;
	list< node > mylist(n);
	node *head = NULL;
	node *temp;
	for(i=0;i<n;i++)
	{
		temp = (node*)malloc(sizeof(node));
		cin >> temp->frequency;		//frequency
		cin >> temp->data;	//value
		temp->right = NULL;
		temp->left = NULL;
		mylist.push_back(*temp);
	}	
	mylist.sort(compare); 
	head = build_tree(head, n, mylist);
	return 0;
}