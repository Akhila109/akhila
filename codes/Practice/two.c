#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct node{
	int pos;
	int value;
	int ans;
	struct node* right;
	struct node* left;
};

typedef struct node node;

int len;
char s1[200000];
char s2[200000];
int count=1000000;

node* insert (int n,int v,int a)
{
	node* pres = (node*) malloc(sizeof(node));
	pres->pos = n;
	pres->value = v;
	pres->ans = a;
	pres->right = NULL;
	pres->left = NULL;
	if(n== (strlen(s1) -1))
		if(pres->ans < count)
			count = pres->ans;
	if(n==strlen(s1))
		return NULL;
	if(a >= count)
		return NULL;
	if(s1[n+1] == '.')
	{
		if(v==1)
			pres->left = insert(n+1,0,a+1);
		else
			pres->left = insert(n+1,0,a);
	}
	if(s2[n+1] == '.')
	{
		if(v==0)
			pres->right = insert(n+1,1,a+1);
		else
			pres->right = insert(n+1,1,a);
	}
	return pres;
}

void preorder(node* node)
{
	if(node == NULL)
		return;
	printf("%d %d %d\n",node->pos, node->value,node->ans);
	preorder(node->left);
	preorder(node->right);
	return;
}

int main()
{
	int tc,i,j,len,stop;
	scanf("%d",&tc);
	for(i=0;i<tc;i++)
	{
		count = 1000000;
		stop = 0;
		scanf("%s",s1);
		scanf("%s",s2);
		len = strlen(s1);
		for(j=0;j<len;j++)
			if(s1[j]== '#' && s2[j] == '#')
			{
				stop = 1;
				printf("No\n");	
				break;
			}
		if(stop == 0)
		{
			printf("Yes\n");
			node* root = insert(-1,-1,0);
		//	preorder(root);					
			printf("%d\n",count);
		}
	}
	return 0;
}

