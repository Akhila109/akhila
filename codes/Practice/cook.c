#include<stdio.h>
#include<math.h>

int istwopower(int n)
{
	while(n!=1)
	{
		if(n%2 != 0)
			return 0;
		n = n/2;
	}
	return 1;
}

int main()
{
	int tc, i, count, result;
	double j,t,num;
	scanf("%d",&tc);
	for(i=0;i<tc;i++)
	{
		count = 0;
		scanf("%lf",&num);
		scanf("%lf",&t);
		while(!istwopower( (int) num))
		{
			if((int)num%2 == 1 && num != 1)
			{
				count ++;
				num = (num-1)/2;
			}
			else if((int) num%2 == 0)
			{
				count ++;
				num = num/2;
			}
		}
		j = t / num;
		result = (int) (log (j) / log(2));
//		printf("%d %lf\n",result, j);
		result = (result < 0) ? result* (-1) : result ;
		count += result;
		printf("%d\n",count);
	}
	return 0;
}
