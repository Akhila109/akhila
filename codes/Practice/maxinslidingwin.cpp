#include <iostream>
#include <deque>
#include<vector>

using namespace std;

int main()
{
	deque<int> window;
	int n,w,i;
	cin >> n;
	cin >> w;
	vector<int> myarray(n);
	for(i=0;i<n;i++)
		cin >> myarray[i] ;
	vector<int> ansarray(n);

	for(i=0;i<n;i++)
	{	
		while(!window.empty() && i - window.front() >= w)
			window.pop_front();

		while(!window.empty() && myarray[i] > myarray[window.back()])
			window.pop_back();
		window.push_back(i);
		if(i >= w-1 )
			ansarray[i + 1 -w] = myarray[window.front()];
		
	}
	for(i=0;i<n+1-w;i++)
		cout << ansarray[i];
	cout << endl;
	return 0;
}
