#include<iostream>
#include<stack>
#include<stdio.h>
#include<string.h>

using namespace std;

int calculate(int a, int b, int c)
{
	if(b == '+')
		return a+c;
	else if(b == '-')
		return a-c;
	else if(b == '*')
		return a*c;
	else if(b == '/')
		return a/c;
}

int Evaluate(char *array)
{
	stack<int> operators;
	stack<int> operands;
	int i, len = strlen(array);
	int a, b, temp;
	for(i=0;i<len;i++)
	{
		if(array[i] == '+' || array[i] == '-') // || array[i] == '-' || array[i] == '*' || array[i] == '/')
		{
			while(!operators.empty())
			{
				temp = operators.top();
				a = operands.top();
				operands.pop();
				b = operands.top();
				operands.pop();
				operands.push( calculate(b, temp, a));
				operators.pop();
			}
			operators.push(array[i]);
		}
		else if(array[i] == '*' || array[i] == '/')
		{
			while(!operators.empty() && (operators.top() == '/' || operators.top() == '*'))
			{
				temp = operators.top();
				a = operands.top();
				operands.pop();
				b = operands.top();
				operands.pop();
				operands.push( calculate(b, temp, a));
				operators.pop();
			}
			operators.push(array[i]);
		}
		else
			operands.push(array[i] - 48);
	}
	while(!operators.empty())
	{
		temp = operators.top();
		a = operands.top();
		operands.pop();
		b = operands.top();
		operands.pop();
		operands.push( calculate(b, temp, a));
		operators.pop();
	}
	return operands.top();
}

int main()
{
	int tc, i, result;
	char array[20];
	scanf("%d", &tc);
	for(i = 0; i < tc; i++)
	{
		scanf("%s", array);
		result = Evaluate(array);
		printf("%d\n", result);
	}
	return 0;
}