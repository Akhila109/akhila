#include<iostream>
#include<cstdio>
#include<stack>

using namespace std;

int *FindingSpanner(int *array, int t)
{
	int i, a;
	stack<int> mystack;
	int result[t];
	for(i = 0; i < t; i++)
		result[i] = 1;
	for(i = 0; i < t; i++)
	{
		while(!mystack.empty() && array[i] > array[mystack.top()])			// = 
		{
			a = mystack.top(); 
			result[i] += result[a];
			mystack.pop();
		//	printf("%d\n", i);
		}
		mystack.push(i);
	}
	return result;
}

int main()
{
	int i, j, tc, t;
	int array[10];
	int *result;
	scanf("%d", &tc);
	for(i = 0; i < tc; i++)
	{
		scanf("%d", &t);
		for(j = 0; j < t; j++)
			scanf("%d", &array[j]);
		result = FindingSpanner(array, t);
		for(j = 0;  j < t; j++)
			printf("%d ", result[j]);
		printf("\n");
	}
	return 0;
}