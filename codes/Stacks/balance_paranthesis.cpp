#include<iostream>
#include<stack>
#include<stdio.h>
#include<string.h>

using namespace std;

int Balance(char *array)
{
	int i, len = strlen(array);
	stack<int> mystack;
	for(i = 0; i < len; i++)
	{
		if(array[i] == '(' || array[i] == '[' || array[i] == '{')
			mystack.push(array[i]);
		else if(!mystack.empty() && array[i] == ')' && mystack.top() == '(')
			mystack.pop();
		else if(!mystack.empty() && array[i] == ']' && mystack.top() == '[')
			mystack.pop();
		else if(!mystack.empty() && array[i] == '}' && mystack.top() == '{')
			mystack.pop();
		else
			return 1;
	}
	if(!mystack.empty())
		return 1;
	return 0;
}

int main()
{
	int tc, i, result;
	char array[20];
	scanf("%d", &tc);
	for(i = 0; i < tc; i++)
	{
		scanf("%s", array);
		result = Balance(array);
		if(result)
			printf("AYYO!!! NOT BALANCED\n");
		else
			printf("HURRAY!!! BALANCED\n");
	}
	return 0;
}