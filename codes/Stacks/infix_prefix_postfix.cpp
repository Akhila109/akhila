/* Simplest ones are infix <--> postfix ..here pop if >= :)
//infix to prefix is infix-> reverse infix -> convert to postfix -> reverse output ..here pop if > only :) */

#include<iostream>
#include<stack>
#include<stdio.h>
#include<string.h>

using namespace std;

char *reverse_string(char *A)
{
	int i = 0;
	for(i = 0; i < strlen(A)/2; i++)
	{
		A[i] = A[strlen(A) - 1 - i];
	}
	return A;
}

char *infix_postfix(char *array)
{
	stack<int> mystack;
	int i, len = strlen(array), j = 0;
	char result[len];
	for(i=0;i<len;i++)
	{
		if(array[i] >= 'A' && array[i] <= 'Z')
			result[j++] = array[i];
		else
		{
			if(array[i] == '(')
				mystack.push(array[i]);
			else if(array[i] == '/')
			{
				while(!mystack.empty() && mystack.top()=='*')
				{
					result[j++] = mystack.top();
					mystack.pop();
				}
				mystack.push(array[i]);
			}
			else if(array[i] == '*')
			{
				while(!mystack.empty() && mystack.top()=='/')
				{
					result[j++] = mystack.top();
					mystack.pop();
				}
				mystack.push(array[i]);
			}
			else if(array[i] == '+' || array[i] == '-')
			{
				while(!mystack.empty() && mystack.top() != '(')
				{
					result[j++] = mystack.top();
					mystack.pop();
				}
				mystack.push(array[i]);
			}
			else if(array[i] == ')')
			{
				while(!mystack.empty() && mystack.top() != '(')
				{
					result[j++] = mystack.top();
					mystack.pop();
				}
				mystack.pop();
			}
		}
	}
	while(!mystack.empty())
	{
		result[j++] = mystack.top();
		mystack.pop();
	}
	result[j] = '\0';
//	printf("%s\n", result);
	return result; 
}

char *postfix_infix(char *A)
{
//	A = reverse_string(A);
	int i = 0, j = 0;
	char result[strlen(A) + 1];
	stack<int> mystack;
	for (i = 0; i < strlen(A); i++)
	{
		if(A[i] >= 'A' && A[i] <= 'Z')
			mystack.push(A[i]);
		if(A[i] == '+' || A[i] == '-' || A[i] == '*' || A[i] == '/')
		{
			if(j == 0)
			{
				if(!mystack.empty())
				{
					result[2] = mystack.top();
					mystack.pop();
					result[1] = A[i];
					if(!mystack.empty())
					{
						result[0] = mystack.top();
						mystack.pop();
						j = 3;
					}
					else
						return NULL;
				}
				else
					return NULL;
			}
			else
			{
				result[j++] = A[i];
				if(!mystack.empty())
				{
					result[j++] = mystack.top();
					mystack.pop();
				}
				else
					return NULL;
			}
		}
	}
	result[j] = '\0';
	return result;
}

int main()
{
	int tc, i, j = 0;
	char array[20], c, d;
	char *result;
	scanf("%d",&tc);
	for(i=0;i<tc;i++)
	{
		scanf("%c", &c);
		scanf("%c", &c);
		scanf("%c", &d);
		for(j = 0; j < 20; j++)
			array[j] = 0;
		j = 0;
		scanf("%s",array);
		if(c == 'i' && d == 'o')
			result = infix_postfix(array);
		if(c == 'o' && d == 'i')
			result = postfix_infix(array);
		while(result[j])
			printf("%c", result[j++]);
		printf("\n");
	}
	return 0;
}