#include<iostream>
#include<cstdio>
#include<stack>

using namespace std;

int FindingArea(int *array, int t)
{
	int i, a;
	stack<int> mystack;
	int max_area, top;
	for(i = 0; i < t; i++)
	{
		if(!mystack.empty())
			top = mystack.top();
		while(!mystack.empty() && array[i] < array[mystack.top()])			// = 
		{
			a = mystack.top();
			mystack.pop();
			if(!mystack.empty())
			{
				if((top-a+1) * array[a] > max_area)
					max_area =  (top-a+1) * array[a];
			}
			else
			{
				if((top+1) * array[a] > max_area)
					max_area =  (top+1) * array[a];
			}		
		}
		mystack.push(i);
	}
	if(!mystack.empty())
			top = mystack.top();
	while(!mystack.empty())			// = 
	{
		a = mystack.top();
		mystack.pop();
		if(!mystack.empty())
		{
			if((top-a+1) * array[a] > max_area)
				max_area =  (top-a+1) * array[a];
		}
		else
		{
			if((top+1) * array[a] > max_area)
				max_area =  (top+1) * array[a];
		}		
	}	
	return max_area;
}

int main()
{
	int i, j, tc, t;
	int array[10];
	int result;
	scanf("%d", &tc);
	for(i = 0; i < tc; i++)
	{
		scanf("%d", &t);
		for(j = 0; j < t; j++)
			scanf("%d", &array[j]);
		result = FindingArea(array, t);
		printf("%d\n",result);
	}
	return 0;
}