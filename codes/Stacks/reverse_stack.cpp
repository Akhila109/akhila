#include<iostream>
#include<stack>
#include<stdio.h>
#include<string.h>

using namespace std;

void insert_bottom(stack<int>& foo, int a)
{
	if(foo.empty())
	{
		foo.push(a);
		return;
	}
	int temp = foo.top();
	foo.pop();
	insert_bottom(foo, a);
	foo.push(temp);
	return;
}

void reverse(stack<int>& foo)
{
	if(foo.empty())
		return;
	int a = foo.top();
	foo.pop();
	reverse(foo);
	insert_bottom(foo, a);
	return;
}

int main()
{
	int tc, i, len, j, result;
	char array[20];
	scanf("%d", &tc);
	for(i = 0; i < tc; i++)
	{
		scanf("%s", array);
		stack<int> mystack;
		len = strlen(array);
		for(j = 0; j < len; j++)
			mystack.push(array[j]);
	
	//	len = strlen(array);
		reverse(mystack);
		while(!mystack.empty())
		{
			printf("%d ", mystack.top());
			mystack.pop();
		}
		printf("\n");
	}
	return 0;
}